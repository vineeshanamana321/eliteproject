import Data.Char
import Data.List

stringTask:: String -> String
stringTask word = concat [replace ( toLower letter ) | letter <- word]

isVowel :: Char -> Bool
isVowel letter = letter `elem` ['a', 'e', 'i', 'o', 'u', 'y']

replace :: Char -> [Char]
replace letter = if isVowel letter then ""
                  else "." ++ [letter]

main = do
    word <- getLine
    putStrLn $ stringTask word
