import Data.List

removeDups :: (Ord a) => [a] -> [a]
removeDups = map head . group . sort

isPossible :: [Int] -> [Int] -> Int -> Bool
isPossible littleX littleY num = removeDups (littleX ++ littleY) == [1..num]

main = do
    num <- getLine
    liX <- getLine
    liY <- getLine
    let x = head (map (read :: String -> Int) . words $ liX)
    let y = head (map (read :: String -> Int) . words $ liY)
    let littleX = tail (map (read :: String -> Int) . words $ liX)
    let littleY = tail (map (read :: String -> Int) . words $ liY)
    print $ isPossible littleX littleY (read num)

