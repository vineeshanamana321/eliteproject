compress :: String -> String
compress word = [head word] ++ show ((length word) - 2) ++ [last word]

compressWord :: String -> String
compressWord word = if length word > 10 then compress word
                    else word

main = do
    word <- getLine
    print $ compressWord word


                
