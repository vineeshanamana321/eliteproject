import Data.Char

lowercaseLen :: String -> Int
lowercaseLen word = length [x | x <- word , isLower x]

uppercaseLen :: String -> Int
uppercaseLen word = length [x | x <- word , isUpper x]

replaceToLower :: String -> String
replaceToLower word = [toLower x | x <- word]

replaceToUpper :: String -> String
replaceToUpper word = [toUpper x | x <- word]

replaceWord :: String -> String
replaceWord word = if lowercaseLen word < uppercaseLen word then replaceToUpper word
                   else replaceToLower word
main = do
    word <- getLine
    putStrLn $ replaceWord word
