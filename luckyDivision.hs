import Data.Char
import Data.List

digits :: Int -> [Int]
digits = map digitToInt . show

removeDups :: (Ord a) => [a] -> [a]
removeDups = map head . group . sort

isLuckyNum :: Int -> Bool
isLuckyNum num = removeDups (digits num) == [4, 7]

main = do
    num <- getLine
    print $ isLuckyNum (read num)
