incWeight :: Int -> Int -> Int -> Int
incWeight limak bob i = if limak == bob then 1
                      else if ((3 ^ i) * limak) > ((2 ^ i) * bob) then i
                      else incWeight limak bob (i + 1)

main = do
    limak <- getLine
    bob   <- getLine
    print $ incWeight (read limak) (read bob) 1
