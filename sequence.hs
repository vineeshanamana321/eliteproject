nextNum :: Int -> Int
nextNum num = if num `mod` 10 == 0 then (num `div` 10)
              else (num - 1)

numSequence :: Int -> Int -> [Int]
numSequence num k = if k == 1 then [nextNum num]
                    else (nextNum num) : numSequence (nextNum num) (k-1) 

main = do
    num <- getLine
    n <- getLine
    print $ (numSequence (read num) (read n)) 
 
